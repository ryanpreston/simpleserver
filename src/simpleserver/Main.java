package simpleserver;

class Main{

  public static void main(String[] args){
    System.out.println("Server Starting\n");
    Server server = Server.getInstance();
    server.ServerListen();
    System.out.println("Server Shutting Down\n");
  }
}