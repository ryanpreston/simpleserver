package simpleserver;

import java.util.HashMap;
import java.util.Map;

public class Posts extends Processor {
    
    private final static Map<String, Posts> postDict = new HashMap();

    private final String postid;
    private final String userid;
    private final String data;
    

    
    public Posts(String postid, String userid, String data) {

        this.postid = postid;
        this.userid = userid;
        this.data   = data;

        postDict.put(postid, this);
    }

    public static Posts getPost(String postid){
        return postDict.get(postid);
    }

    public String getPostid(){
        return postid;
    }
    public String getUserid(){
        return userid;
    }
    public String getData() {
        return data;
    }
}
   