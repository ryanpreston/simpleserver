package simpleserver;

import java.util.HashMap;
import java.util.Map;

public class User extends Processor{

    private final static Map<String,User> userDict = new HashMap();

    private final String username;
    private final String userid;


    public User(String username, String userid){
        this.username = username;
        this.userid   = userid;

        userDict.put(userid, this);
    }

    public static User getUser(String userid){
        return userDict.get(userid);
    }

    public String getUsername(){
        return username;
    }
    public String getUserid(){
        return userid;
    }
}
