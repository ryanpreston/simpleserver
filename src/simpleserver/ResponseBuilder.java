package simpleserver;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ResponseBuilder {

    private String status = "\"status\": \"";
    private String timeStamp = "    \"timestamp\": \"";
    private String entries = "    \"entries\": ";
    private String data = "    \"data\" : ";

    public void setStatus(String status){
        this.status += status;
        this.status += "\",<br>";
    }
    public void setEntries(String entries){
        this.entries += entries;
        this.entries += ",<br>";
    }
    public void setData(String data){
        this.data += data;
    }
    public String getStatus(){
        return status;
    }
    public String getTimeStamp(){
        timeStamp += new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        timeStamp += "\",<br>";

        return timeStamp;
    }
    public String toString(){
        String response;
        response = "{<br>";
        response += status;
        response += timeStamp;
        response += entries;
        response += data;
        response += "<br>}";

        return response;
    }
}
