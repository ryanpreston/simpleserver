package simpleserver;

import java.util.HashMap;
import java.util.Map;

public class Comments extends Processor {

    private final static Map<String, Comments> commentDict = new HashMap();

    private final String postid;
    private final String userid;
    private final String data;

    
    public Comments(String postid, String userid, String data){

        this.postid = postid;
        this.userid = userid;
        this.data   = data;

        commentDict.put(postid, this);
    }

    public static Comments getComment(String postid){
        return commentDict.get(postid);
    }

    public String getPostid(){
        return postid;
    }
    public String getUserid(){
        return userid;
    }
    public String getData(){
        return data;
    }
    
}

    



