package simpleserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import com.google.gson.*;
import java.io.FileNotFoundException;

class Server {
    private static Server ourInstance;
    private ServerSocket port;
    private Socket request;


    static Server getInstance() {

        if (ourInstance == null) {
            ourInstance = new Server();
        }
        return ourInstance;
    }

    private Server() {
        port = null;
        request = null;
    }



    //Listens for a request from a user
    //Makes call(s) to: parseRequest()
    void ServerListen() {
        System.out.println("ServerListen()");

        Gson gson = new Gson();
        BufferedReader br;
        JsonObject obj;
        JsonParser jsonParser;
        User[] users = null;
        Posts[] posts = null;
        Comments[] comments = null;

        try{
            br = new BufferedReader(new FileReader("data.json"));
            jsonParser = new JsonParser();
            obj = jsonParser.parse(br).getAsJsonObject();

            users = gson.fromJson(obj.get("users"), User[].class);
            posts = gson.fromJson(obj.get("posts"), Posts[].class);
            comments = gson.fromJson(obj.get("comments"), Comments[].class);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

        try {
            port = new ServerSocket(1299);
            System.out.println("Opened socket " + 1299);
            while (true) {

                // keeps listening for new clients, one at a time
                try {
                    request = port.accept(); // waits for client here
                } catch (IOException e) {
                    System.out.println("Error opening socket");
                    System.exit(1);
                }

                InputStream stream = request.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(stream));
                String[] parsedRequest = null;
                try {

                    // read the first line to get the request method, URI and HTTP version
                    String line = in.readLine();
                    System.out.println("----------REQUEST START---------");

                   parsedRequest = parseRequest(line);

                    System.out.println("----------REQUEST END---------\n\n");
                } catch (IOException e) {
                    System.out.println("Error reading request");
                    System.exit(1);
                }

                BufferedOutputStream out = new BufferedOutputStream(request.getOutputStream());
                PrintWriter writer = new PrintWriter(out, true);  // char output to the client
                ResponseBuilder response = new ResponseBuilder();
                if(parsedRequest.length == 1){
                    respondWithAllData(response, parsedRequest[0], gson, users, posts, comments);
                }
                else{
                    respondWithSpecificData(response, parsedRequest, gson, users, posts, comments);
                }

                // every response will always have the status-line, date, and server name
                writer.println("HTTP/1.1 200 OK");
                writer.println("Server: TEST");
                writer.println("Connection: close");
                writer.println("Content-type: text/html");
                writer.println("");

                // Body of our response
                writer.println(response.toString());

                request.close();
            }
        } catch (IOException e) {
            System.out.println("Error opening socket");
            System.exit(1);
        }
    } // End ServerListen()


    //Parses the request received from the user
    //Makes call(s) to: generateResponse()
    private String[] parseRequest(String userRequest) {

        if(userRequest.indexOf('?') == -1){
            String parsedRequest = removeSpaces(userRequest);
            String endPoint = getEndpoint(parsedRequest);
            String[] request = {endPoint};

            return request;
        }
        else {
            String parsedRequest = removeSpaces(userRequest);
            String endPoint = getEndpoint(parsedRequest);
            String requestParameters = getRequestParameters(parsedRequest);
            String[] parameters = requestParameters.split("&");
            String[] id = getParameterID(endPoint, parameters, parameters.length);
            long requestNumber = parsedRequest.chars().filter(num -> num == '&').count() + 1;

            return id;
        }
    }
    //Removes the spaces to get the user requests
    private String removeSpaces(String str){

        int spaceIndex = str.indexOf(' ');
        String parsedRequest = str.substring(spaceIndex + 1);
        spaceIndex = parsedRequest.indexOf(' ');
        parsedRequest = parsedRequest.substring(0, spaceIndex);

        return parsedRequest;
    }
    //Isolates the endpoint from the user request
    private String getEndpoint(String str){

        int slashIndex = str.indexOf('/');
        int questionMarkIndex = str.indexOf('?');
        if(questionMarkIndex == -1){
            return str.substring(slashIndex + 1);
        }
        return str.substring(slashIndex + 1, questionMarkIndex);
    }
    //Creates an array of the request parameters
    private String getRequestParameters(String str){
        int questionMarkIndex = str.indexOf('?');
        return str.substring(questionMarkIndex + 1);
    }
    //Creates an array of the request ids
    private String[] getParameterID(String endpoint, String[] str, int size){
        String[] ids = new String[size + 1];
        ids[0] = endpoint;
        for (int i = 1; i <= str.length; i++){
            ids[i] = str[i-1].substring(str[i-1].indexOf('=') + 1);
        }

        return ids;
    }
    private void respondWithAllData(ResponseBuilder response, String endpoint, Gson gson,
                                    User[] users, Posts[] posts, Comments[] comments){
        response.setStatus("OK");
        response.getTimeStamp();
        if(endpoint.equals("users")) {
            response.setEntries(Integer.toString(users.length));
            response.setData(gson.toJson(users));
        }
        else if (endpoint.equals("posts")) {
            response.setEntries(Integer.toString(posts.length));
            response.setData(gson.toJson(posts));
        }
        else if (endpoint.equals("comments")) {
            response.setEntries(Integer.toString(comments.length));
            response.setData(gson.toJson(comments));
        }
        else {
            response.setEntries("0");
            response.setData("No Data");
        }
    }
    private void respondWithSpecificData(ResponseBuilder response, String[] parsedRequest, Gson gson,
                                         User[] users, Posts[] posts, Comments[] comments){

        response.setStatus("OK");
        response.getTimeStamp();
        response.setData("[<br>");

        if(parsedRequest[0].equals("users")) {
            int count = 0;
            for (int i = 1; i < parsedRequest.length; i++ ){
                if(Integer.parseInt(parsedRequest[i]) < users.length) {
                    response.setData(gson.toJson(users[Integer.parseInt(parsedRequest[i])]));
                }
                else{
                    response.setData("{\"Invalid ID: " + parsedRequest[i] + "\"}");
                }
                if(i != (parsedRequest.length - 1)) {
                    response.setData(",");
                }
                count++;
            }
            response.setEntries(Integer.toString(count));
        }
        else if (parsedRequest[0].equals("posts")) {
            int count = 0;
            Posts post;
            for (int i = 1; i < parsedRequest.length; i++ ){

                if(Integer.parseInt(parsedRequest[i]) < posts.length) {
                    response.setData(gson.toJson(posts[Integer.parseInt(parsedRequest[i])]));
                }
                else{
                    response.setData("{\"Invalid ID: " + parsedRequest[i] + "\"}");
                }
                count++;
            }
            response.setEntries(Integer.toString(count));
        }
        else if (parsedRequest[0].equals("comments")) {
            int count = 0;
            for (int i = 1; i < parsedRequest.length; i++ ){
                if(Integer.parseInt(parsedRequest[i]) < comments.length) {
                    response.setData(gson.toJson(comments[Integer.parseInt(parsedRequest[i])]));
                }
                else{
                    response.setData("{\"Invalid ID: " + parsedRequest[i] + "\"}");
                }
                count++;
            }
            response.setEntries(Integer.toString(count));
        }
        else {
            response.setEntries("0");
            response.setData("No Data");
        }
        response.setData("<br>],<br>");

    }
}

